package ru.shchurin.tm.entity;

public class Task {

    private int id;

    private String name;

    private int projectId;

    static int count = 1;

    public Task(String name) {
        this.name = name;
        this.id = count++;
    }

    public Task() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
