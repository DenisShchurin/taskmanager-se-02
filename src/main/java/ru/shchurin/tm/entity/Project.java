package ru.shchurin.tm.entity;

public class Project {

    private int id;

    private String name;

    static int count = 1;

    public Project(String name) {
        this.name = name;
        this.id = count++;
    }

    public Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
