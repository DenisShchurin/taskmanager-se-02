package ru.shchurin.tm;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.service.ProjectTaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main( String[] args ) throws IOException {

        List<Project> projects = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        ProjectTaskService projectTaskService = new ProjectTaskService(projects, tasks);

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        Boolean exit = false;

        while (!exit) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String command = reader.readLine();

            if (command.equals("project-clear")) {
                projectTaskService.deleteAllProject();

            } else if (command.equals("project-create")) {
                projectTaskService.createProject();

            } else if (command.equals("project-list")) {
                projectTaskService.listProject();

            } else if (command.equals("project-remove")) {
                projectTaskService.deleteProject();

            } else if (command.equals("task-clear")) {
                projectTaskService.deleteAllTasks();

            } else if (command.equals("task-create")) {
                projectTaskService.createTask();

            } else if (command.equals("task-list")) {
                projectTaskService.listTask();

            } else if (command.equals("task-remove")) {
                projectTaskService.deleteTask();

            } else if (command.equals("help")) {
                projectTaskService.showAllCommand();

            } else if (command.equals("exit")) {

                exit = true;

            } else {
                System.out.println("You entered wrong command");
            }
        }
    }
}
