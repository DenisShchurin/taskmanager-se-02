package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ProjectTaskService {

    private List<Project> projects;

    private List<Task> tasks;

    public ProjectTaskService() {
    }

    public ProjectTaskService(List<Project> projects, List<Task> tasks) {
        this.projects = projects;
        this.tasks = tasks;
    }

    public void deleteAllProject() {
        projects.clear();
        System.out.println("[ALL PROJECT REMOVE]");
    }

    public void deleteAllTasks() {
        tasks.clear();
        System.out.println("[ALL TASK REMOVE]");
    }

    public void createProject() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = reader.readLine();
        projects.add(new Project(name));
        System.out.println("[OK]");
    }

    public void createTask() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = reader.readLine();
        tasks.add(new Task(name));
        System.out.println("[OK]");
    }

    public void listProject() {
        System.out.println("[PROJECT LIST]");
        for (Project project : projects) {
            System.out.println(project.getId() + ". " + project.getName());
        }
    }

    public void listTask() {
        System.out.println("[TASK LIST]");
        for (Task task : tasks) {
            System.out.println(task.getId() + ". " + task.getName());
        }
    }

    public void showAllCommand() {
        System.out.println("help: Show all commands.");
        System.out.println("project-clear");
        System.out.println("project-create");
        System.out.println("project-list");
        System.out.println("project-remove");
        System.out.println("task-clear");
        System.out.println("task-create");
        System.out.println("task-list");
        System.out.println("task-remove");
    }

    public void deleteProject() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int index = Integer.parseInt(reader.readLine());
        projects.remove(index);
        System.out.println("[PROJECT REMOVE]");
    }

    public void deleteTask() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int index = Integer.parseInt(reader.readLine());
        tasks.remove(index);
        System.out.println("[TASK REMOVE]");
    }
}
