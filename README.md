## Taskmanager-2
Simple CRUD application


## Prerequisites
Windows 10
JRE 8


## Technologies
MAVEN, GITLAB, GIT-IGNORE, MANIFEST.MF, MAVEN-JAR-PLUGIN


## Authors
Denis Shchurin
denisshchurin@gmail.com


## Command for building project

```
mvn compile
```


## Command for execution project

```
mvn exec:java -Dexec.mainClass="ru.shchurin.tm.App"
```






